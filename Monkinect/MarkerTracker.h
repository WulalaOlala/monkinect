#define _USE_MATH_DEFINES

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <libconfig.h++>
#include <omp.h>
#include <iostream>
#include <vector>
#include <cmath>

#include "Marker.h"

using namespace std;
using namespace cv;
using namespace libconfig;

class MarkerTracker{

public:

	MarkerTracker();
	MarkerTracker(int, int, int);
	MarkerTracker(const Config &cfg);

	void add(Point seed, const Mat &colorImg, const Mat &depthMap);
	void clear();


	void staticTrack(const Mat &hsvImg, const Mat &depthMap);
	void munkresTrack(const Mat &hsvImg, const Mat &depthMap); //staticTrack with munkres LAP
	void adaptiveTrack(const Mat &hsvImg, const Mat &depthMap); //use CAMSHIFT algorithm


	void update(int skip, int lowpass, int highpass);

	bool reload(const Config &cfg);

	//accessors
	const vector<vector<Marker>>& groups() const;
	int markerCount() const;

	bool usingCamshift() const;

protected:

	Mat ThresholdHue(const Scalar &hue, const Mat &hsvImg);
	Mat ThresholdHSV(const Scalar &hsv, const Mat &hsvImg);
	Mat CreateHist(const Rect &selection, const Mat &mask, const Mat &hsvImg);

	Mat ThresholdDepth( const Mat &depthMap); //static track
	Mat ThresholdDepth( const Mat &depthMap, double depthSeed, double searchRadius);

	//Detect objects and extract blob data (for shape fitting, moment extraction, etc)
    void Blobs(const Mat &binary, const Mat &depthMap, vector <vector<Point2d>> &blobs, vector<Rect> &rects, vector<double> &depths);

	//Detect objects, only get fitted rectangle
	void DetectObjects(const Mat &binary, const Mat &depthMap, vector<Rect> &rects, vector<double> &depths);
	//Detect objects, but also get their hsv properties
	void DetectObjects(const Mat &binary, const Mat&hsvImg, const Mat &depthMap, vector<Rect> &rects, vector<Scalar> &scalars, vector<double> &depths);

	void SeedObject(Point seed, const Mat &hsvImg, const Mat &depthMap, vector <vector<Point2d>> &blobs, vector<Rect> &rects, vector<double> &depths, Scalar hue);
	void SeedObject(Point seed, const Mat &binary, const Mat &hsvImg, const Mat &depthMap,  vector<Rect> &rects, vector<double> &depths, Scalar hue);


private:
	
	bool m_camshift; //use the camshift algorithm if true, connected components if false

	int markerColors;

	//blur selection
	int m_blur;

	//morphology mods
	  //morphology kernels
	Mat m_kernelOpen; 
	Mat m_kernelClose;
	  //number of iterations to appply
	int m_iterOpen;
	int m_iterClose;

	//hue modifiers
	int m_Hlow;
	int m_High;

	//Value Threshold (for static tracking)
	int m_Vmin;
	int m_Vmax;

	//Value bounds (for adaptive tracking)
	int m_Vlow;
	int m_Vhigh;
	//Sat threshold
	int m_Smin;
	int m_Smax;
	//Sat bounds
	int m_Slow;
	int m_Shigh;

	//THRESHOLDING - performance
	int m_skip; //for systematic subsampling
	int m_lowpass;
	int m_highpass;

	//Depth Thresholding (when using staticTrack
	int m_Dmin;
	int m_Dmax;

	//MARKER -properties for marker init
	int m_markerCount; //marker counter
	int m_slidingFrames;
	double m_markerRadius;
	double m_markerMaxRadius;

	//hue containers
	vector<Scalar> m_hueValues;

	//class containers
	vector<vector<Marker>> m_Groups;

};