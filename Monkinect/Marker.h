#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "MedianFilter.h"

#include <vector>
#include <deque>
#include <iostream>
#include <cmath>

using namespace std;
using namespace cv;

class Marker{

public:
	Marker();
	Marker(int);
	Marker(Rect, double, Scalar, int, int);
	Marker(Rect, double, Scalar, Mat, int, int);
	
	//Skip blob data for speed
	void update(Rect rect);
	void update(Rect rect, double depth);
	void update(Rect rect, double depth, Scalar scalars);

	//Get the Rotated Rect if using CAMSHIFT
	void update(RotatedRect rotated, Rect rect, double depth);

	//update blob data for not speed (used to get moments and center of mass, if necessary)
	void update(vector<Point2d> blob, Rect rect, double depth);


	void predict();

	Point3d center() const;
	Point3d predictedCenter() const;
	Point3d filteredCenter() const;

	double depth() const;
	double size() const;
	
	double searchRadius() const;
	void searchRadius (double radius);

	void raiseOcclusion ();
	void resetOcclusion ();

	int estimateCost(Point3d targetPosition, int targetSize);

	Rect rect() const;
	void rect(Rect rect);

	bool isOccluded() const;
	int framesOccluded() const;
	int hueValue() const;
	int ID() const;

	const Scalar& markerHSV() const;
	const Mat& markerHist() const;
	const RotatedRect& rotatedRect() const;

protected:
	void initKalman();
	void initMedian();
	void estimateCost();

private:

	bool m_occluded; //is the marker occluded?

	int  m_id; //number of marker with respect to other markers
	int  m_hueV; //hue value that represents that marker
	int  m_framesOccluded; //number of frames the marker has been occluded
	int  m_framesSliding; //number of frames to use for sliding window

	Rect m_rect; //rectangle containing the marker
	Point3d m_center;    //center of mass of marker
	Point3d m_predicted; //predicted center of marker (kalman filter)
	Point3d m_median;    //median center of marker (median filter)

	double m_depth;   //depth of marker in mm
	double m_size;    //marker size on screen
	double m_radius;  //search radius

	Scalar m_hsv; //HSV vector representing median properties of the object
	Mat    m_hist; //color histogram for use with camshift

	RotatedRect m_rotated;
	
	//Movement Filters
	MedianFilter m_medianfilter;
	KalmanFilter m_kalmanfilter;



};