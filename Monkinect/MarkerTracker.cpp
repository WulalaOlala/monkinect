#include "MarkerTracker.h"
#include "hungarian.h"

MarkerTracker::MarkerTracker()
{
	m_skip = 10;
	m_lowpass = 400;
	m_highpass = 40000;
	m_blur = 0;
	m_markerCount = 0;

	m_iterOpen = 1;
	m_iterClose = 3;

	m_Hlow = 2;
	m_High = 3;
	m_Smin = 100;
	m_Smax = 255;
	m_Vmin = 100;
	m_Vmax = 255;

	m_slidingFrames = 5;

	m_markerRadius  = 10000;
	m_markerMaxRadius = 55000;

	m_kernelClose = getStructuringElement(MORPH_RECT, Size(3, 3)); 
	m_kernelOpen = getStructuringElement(MORPH_RECT, Size(3, 3)); 

	
}

MarkerTracker::MarkerTracker(int skip, int lowpass, int highpass = 40000)
{
	m_skip = skip;
	m_lowpass = lowpass;
	m_highpass = highpass;
	m_blur = 0;
	m_markerCount = 0;

	float hranges[] = {0,180};


	m_iterOpen = 1;
	m_iterClose = 3;

	m_Hlow = 2;
	m_High = 3;
	m_Smin = 100;
	m_Smax = 255;
	m_Vmin = 100;
	m_Vmax = 255;

	m_Dmin = 500;
	m_Dmax = 2000;

	m_slidingFrames = 5;

	m_markerRadius  = 10000;
	m_markerMaxRadius = 55000;

	m_kernelClose = getStructuringElement(MORPH_RECT, Size(3, 3)); 
	m_kernelOpen = getStructuringElement(MORPH_RECT, Size(3, 3)); 


}

MarkerTracker::MarkerTracker(const Config &cfg)
{
	//constructor with config filer.
	m_markerCount = 0;
	m_kernelClose = getStructuringElement(MORPH_RECT, Size(3, 3)); 
	m_kernelOpen = getStructuringElement(MORPH_RECT, Size(3, 3)); 


	try
	{
		const Setting& root = cfg.getRoot();

		const Setting &tracker = root["tracker"];

		tracker.lookupValue("skip", m_skip);
		tracker.lookupValue("lowpass", m_lowpass);
		tracker.lookupValue("highpass", m_highpass);
		tracker.lookupValue("blur", m_blur);

		tracker.lookupValue("open", m_iterOpen);
		tracker.lookupValue("close", m_iterClose);
		tracker.lookupValue("window", m_slidingFrames);

		tracker.lookupValue("camshift", m_camshift);

		const Setting &hue = root["tracker"]["hue"];

		hue.lookupValue("low", m_Hlow);
		hue.lookupValue("high", m_High);
	
		const Setting &satur = root["tracker"]["saturation"];

		satur.lookupValue("min", m_Smin);
		satur.lookupValue("max", m_Smax);
		satur.lookupValue("low", m_Slow);
		satur.lookupValue("high", m_Shigh);

		const Setting &value = root["tracker"]["value"];

		value.lookupValue("min", m_Vmin);
		value.lookupValue("max", m_Vmax);
		value.lookupValue("low", m_Vlow);
		value.lookupValue("high", m_Vhigh);

		const Setting &depth = root["tracker"]["depth"];

		depth.lookupValue("min", m_Dmin);
		depth.lookupValue("max", m_Dmax);

		const Setting &marker = root["marker"];
		marker.lookupValue("radius", m_markerRadius);
		marker.lookupValue("maxradius", m_markerMaxRadius);
	}
	catch(const SettingNotFoundException &nfex)
	{
    // Ignore.
	}

}

void MarkerTracker::update(int skip, int lowpass, int highpass = 40000)
{
	m_skip = skip;
	m_lowpass = lowpass;
	m_highpass = highpass;
}

bool MarkerTracker::reload(const Config &cfg)
{
	//reload values
	m_markerCount = 0;

	try
	{
		const Setting& root = cfg.getRoot();

		const Setting &tracker = root["tracker"];

		tracker.lookupValue("skip", m_skip);
		tracker.lookupValue("lowpass", m_lowpass);
		tracker.lookupValue("highpass", m_highpass);
		tracker.lookupValue("blur", m_blur);

		tracker.lookupValue("open", m_iterOpen);
		tracker.lookupValue("close", m_iterClose);
		tracker.lookupValue("window", m_slidingFrames);

		tracker.lookupValue("camshift", m_camshift);

		const Setting &hue = root["tracker"]["hue"];

		hue.lookupValue("low", m_Hlow);
		hue.lookupValue("high", m_High);
	
		const Setting &satur = root["tracker"]["saturation"];

		satur.lookupValue("min", m_Smin);
		satur.lookupValue("max", m_Smax);
		satur.lookupValue("low", m_Slow);
		satur.lookupValue("high", m_Shigh);

		const Setting &value = root["tracker"]["value"];

		value.lookupValue("min", m_Vmin);
		value.lookupValue("max", m_Vmax);
		value.lookupValue("low", m_Vlow);
		value.lookupValue("high", m_Vhigh);

		const Setting &marker = root["marker"];
		marker.lookupValue("radius", m_markerRadius);
		marker.lookupValue("maxradius", m_markerMaxRadius);
		
		
	return true;

	}
	catch(const SettingNotFoundException &nfex)
	{
		return false;
    // Ignore.
	}


}

Scalar getHSV(Point seed, const Mat &hsvImg)
{
	//get the hue you clicked on
	Scalar s = hsvImg.at<Vec3b>(seed);

	return s;

}

template <typename T>
void freeVec( T & t )
{
    T tmp;
    t.swap( tmp );
}

template <class T>
inline double getDistance(T & t1, T & t2)
{
	//distance formula for comparison, no sqrt
	//OpenCV has a pow, disambiguating

	return (std::pow((t2.x - t1.x),2) + 
		    std::pow((t2.y - t1.y),2) +
			std::pow((t2.z - t1.z),2)); 
}

Point2d getRectCenter(Rect rect)
{
	return (Point2d(rect.tl().x + rect.width/2, rect.tl().y + rect.height/2 ));
}

Rect getRectfromCenter(Point center, int width, int height)
{
	return Rect(Point(center.x - width/2, center.y - height/2), Point(center.x + width/2, center.y + height/2));
}

Point getRectCenterPoint(Rect rect)
{
	return (Point(rect.tl().x + rect.width/2, rect.tl().y + rect.height/2 ));
}

Point3d getCentroid(Rect rect, double depth)
{
	Point2d center = getRectCenter(rect);
	return Point3d(center.x, center.y, depth);
}

void MarkerTracker::clear()
{
	m_hueValues.clear();
	freeVec(m_hueValues);
	m_Groups.clear();
	freeVec(m_Groups);

	m_markerCount = 0; //reset marker count to zero
	
}

double vectorMedian(vector<int> scores)
{
  double median;
  size_t size = scores.size();
  sort(scores.begin(), scores.end());


  if (size  % 2 == 0)
  {
	  median = (scores[size / 2 - 1] + scores[size / 2]) / 2;
  }
  else 
  {
	  median = scores[size / 2];
  }

  return median;
}

vector<int> extractDepth(Rect newRect, const Mat &depthMap)
{
	vector<int> depth_blob;

	for(int i=newRect.y; i < (newRect.y+newRect.height); i++) 
	{
		for(int j=newRect.x; j < (newRect.x+newRect.width); j++) 
		{
			depth_blob.push_back(depthMap.at<__int16>(i,j));
		}
	}

	return depth_blob;
}



//THRESHOLDING AND NOISE REMOVAL
void FilterImage(const Mat &hsvImg, int BLUR = 0)
{
	// blurs, run this only once

	switch(BLUR)
	{
	case 0:
		//no blur
	case 1:
		blur(hsvImg, hsvImg, Size(3, 3), Point(-1,-1));
	case 2:
		medianBlur(hsvImg, hsvImg, 3);//has to be odd
	case 3:
	    //gaussianblur size must be positive and odd
		GaussianBlur(hsvImg, hsvImg, Size(3, 3), 0, 0, BORDER_DEFAULT);
	default:
		;
		//no blur
	}
		//imshow("hsv", hsvImg);
}

Mat MarkerTracker::ThresholdHue(const Scalar &hue, const Mat &hsvImg)
{
	Mat binary;

	//get the hue range you clicked on
	Scalar lower(hue[0]-m_Hlow, m_Smin, m_Vmin);
	Scalar upper(hue[0]+m_High, m_Smax, m_Vmax);
	//threshold, then dilate to remove noise
	inRange(hsvImg, lower, upper, binary); 

	//tweaking to see which parameter works best
	morphologyEx(binary, binary, MORPH_CLOSE, m_kernelClose, cv::Point(-1,-1), m_iterClose); //close removes dark holes and joins the marker together
	morphologyEx(binary, binary, MORPH_OPEN, m_kernelOpen, cv::Point(-1,-1), m_iterOpen);
	dilate(binary, binary, m_kernelOpen, cv::Point(-1,-1), m_iterOpen);

	//imshow("s", binary);

	return binary;
}

Mat MarkerTracker::ThresholdHSV(const Scalar &hsv, const Mat &hsvImg)
{
	Mat binary;
	//get the hue range you clicked on
	Scalar lower(hsv[0]-m_Hlow, hsv[1] - m_Slow, hsv[2] - m_Vlow);
	Scalar upper(hsv[0]+m_High, hsv[1] + m_Shigh, hsv[2] + m_Vhigh);
	//threshold, then dilate to remove noise
	inRange(hsvImg, lower, upper, binary); 

	//tweaking to see which parameter works best
	morphologyEx(binary, binary, MORPH_CLOSE, m_kernelClose, cv::Point(-1,-1), m_iterClose); //close removes dark holes and joins the marker together
	morphologyEx(binary, binary, MORPH_OPEN, m_kernelOpen, cv::Point(-1,-1), m_iterOpen);
	dilate(binary, binary, m_kernelOpen, cv::Point(-1,-1), m_iterOpen);

	//imshow("s", binary);

	return binary;
}

Mat MarkerTracker::ThresholdDepth( const Mat &depthMap)
{
	Mat binary;

	//Depth-based segmentation
	//the lower and upperbounds should be associated with the searchRadius no?
	//constant bounds
	Scalar lower (m_Dmin);
	Scalar upper (m_Dmax);

	inRange(depthMap, lower, upper, binary);

	//imshow("s", binary);

	return binary;
}

Mat MarkerTracker::ThresholdDepth( const Mat &depthMap, double depthSeed, double searchRadius)
{
	Mat binary;
	//Depth-based segmentation
	//Dynamic bounds
	double bound = std::ceil(searchRadius/250.0);
	Scalar lower (depthSeed - bound);
	Scalar upper (depthSeed + bound);

	inRange(depthMap, lower, upper, binary);
	
	//imshow("s", binary);

	return binary;
}

Mat MarkerTracker::CreateHist(const Rect &selection, const Mat &mask, const Mat &hsvImg)
{
	Mat hue, hist;

	int hsize = 16;
	float hranges[] = {0,180};
    const float *phranges = hranges;

	int ch[] = {0, 0};
	hue.create(hsvImg.size(), hsvImg.depth());
	mixChannels(&hsvImg, 1, &hue, 1, ch, 1);

	Mat roi(hue, selection), maskroi(mask, selection);
	calcHist(&roi, 1, 0, maskroi, hist, 1, &hsize, &phranges);
	normalize(hist, hist, 0, 255, CV_MINMAX);

	return hist;

}

//DETECTION FUNCTIONS

void MarkerTracker::Blobs(const Mat &binary, const Mat &depthMap, vector <vector<Point2d>> &blobs, vector<Rect> &rects, vector<double> &depth)
{
	//this function finds all the blobs of a given hue value
	
	Mat label_image;
	binary.convertTo(label_image, CV_32FC1);

	int label_count = 2; // starts at 2 because 0,1 are used already
 
		//look through image with systematic subsampling

		for(int y = rand() % 10; y < label_image.rows; y+=m_skip) 
		{
			for(int x = rand() % 10; x < label_image.cols; x+=m_skip) 
			{
				if(label_image.at<float>(y,x) != 255) 
				{continue;}

				Rect newRect;
				floodFill(label_image, Point(x,y), Scalar(label_count), 
								&newRect, Scalar(0), Scalar(0), 8);

						
				vector<Point2d> image_blob;
				vector<int> depth_blob;
 
				for(int i=newRect.y; i < (newRect.y+newRect.height); i++) 
				{
					for(int j=newRect.x; j < (newRect.x+newRect.width); j++) 
					{
						if(label_image.at<float>(i,j) != label_count) 
						{
							continue;
						}

						image_blob.push_back(Point2d(j,i));
						depth_blob.push_back(depthMap.at<__int16>(i,j));
					}
				}
	
				//if (newRect.area() > m_lowpass && newRect.area() < m_highpass)
				if (image_blob.size() > m_lowpass && image_blob.size() < m_highpass)
				{
					blobs.push_back(image_blob);
					rects.push_back(newRect);
					depth.push_back(vectorMedian(depth_blob));
				}

			}
		}	

}

void MarkerTracker::DetectObjects(const Mat &binary, const Mat &depthMap, vector<Rect> &rects, vector<double> &depths)
{
	//this function finds all the objects from a binary image, and extracts their 3D coordinates using depth informtion
	Mat label_image;
	binary.convertTo(label_image, CV_32FC1);

	int label_count = 2; // starts at 2 because 0,1 are used already
 
		//look through image with systematic subsampling

		for(int y = rand() % m_skip; y < label_image.rows; y+=m_skip) 
		{
			for(int x = rand() % m_skip; x < label_image.cols; x+=m_skip) 
			{
				if(label_image.at<float>(y,x) != 255) 
				{continue;}

				Rect newRect;
				floodFill(label_image, Point(x,y), Scalar(label_count), 
								&newRect, Scalar(0), Scalar(0), 8);

						
				vector<int> depth_blob;
 
				for(int i=newRect.y; i < (newRect.y+newRect.height); i++) 
				{
					for(int j=newRect.x; j < (newRect.x+newRect.width); j++) 
					{
						if(label_image.at<float>(i,j) != label_count) 
						{
							continue;
						}
						depth_blob.push_back(depthMap.at<__int16>(i,j));
					}
				}
	
				if (newRect.area() > m_lowpass && newRect.area() < m_highpass)
				{
					rects.push_back(newRect);
					depths.push_back(vectorMedian(depth_blob));
				}

			}
		}	

}

void MarkerTracker::DetectObjects(const Mat &binary, const Mat&hsvImg, const Mat &depthMap, vector<Rect> &rects, vector<Scalar> &scalars, vector<double> &depths)
{
	//this function finds all the objects from a binary image, and extracts their 3D coordinates using depth informtion
	//adding an hsvimg will overload the function to get hsv properties
	
	Mat label_image;
	binary.convertTo(label_image, CV_32FC1);

	int label_count = 2; // starts at 2 because 0,1 are used already
 
		//look through image with systematic subsampling

		for(int y = rand() % m_skip; y < label_image.rows; y+=m_skip) 
		{
			for(int x = rand() % m_skip; x < label_image.cols; x+=m_skip) 
			{
				if(label_image.at<float>(y,x) != 255) 
				{continue;}

				Rect newRect;
				floodFill(label_image, Point(x,y), Scalar(label_count), 
								&newRect, Scalar(0), Scalar(0), 8);


				if (newRect.area() > m_lowpass && newRect.area() < m_highpass)
				{
					//use reserve...use list?
					vector<int> depth_blob(0, newRect.area());
					//vector<int> hue_blob(0, newRect.area());
					vector<int> sat_blob(0, newRect.area());
					vector<int> val_blob(0, newRect.area());

					//selective subsampling here?
					int skip_t = m_skip / 2;

					for(int i=newRect.y; i < (newRect.y+newRect.height); i+=skip_t) 
					{
						for(int j=newRect.x; j < (newRect.x+newRect.width); j+=skip_t) 
						{
							if(label_image.at<float>(i,j) != label_count) 
							{
								continue;
							}
							//hue_blob.push_back(hsvImg.at<Vec3b>(i,j)[0]);
							sat_blob.push_back(hsvImg.at<Vec3b>(i,j)[1]);
							val_blob.push_back(hsvImg.at<Vec3b>(i,j)[2]);
							depth_blob.push_back(depthMap.at<__int16>(i,j));
						}
					}
					rects.push_back(newRect);

					depths.push_back(vectorMedian(depth_blob));
					scalars.push_back(Scalar(0, vectorMedian(sat_blob), vectorMedian(val_blob)));
				}
			}
		}	

}


//TRACKING FUNCTIONS

//use this function to derive the z coordinate as well.
void MarkerTracker::staticTrack(const Mat &hsvImg, const Mat &depthMap)
{

	FilterImage(hsvImg, m_blur);

	#pragma omp parallel for shared(hsvImg)
	for(int i = 0; i < m_Groups.size(); i++)
	{
		Mat depthMask, hueMask, binary;
		//iterate over the stored hueValuse
		//segment the image based on color
		vector<Rect> trackedRects;

		depthMask = ThresholdDepth(depthMap);

		hsvImg.copyTo(hueMask, depthMask);
		binary = ThresholdHue(m_hueValues[i], hueMask);

		int markers = m_Groups[i].size();

		for(int m = 0; m < markers; m++)
		{

			vector<Rect> newRects; //newly returned rects
			vector<double> newDepths; //new center of masses( z  for a given rect)
			//vector<vector<Point2d>> newBlobs; //new blob data (x and y for a given rect)

			m_Groups[i][m].predict();
			Point3d predictedPt = m_Groups[i][m].predictedCenter();

			//fucking cool. Further segment the image by clearing out any pixels in a 3D space not predicted by Kalman.
			//searchRadius should dictate the space looked at

			DetectObjects(binary, depthMap, newRects, newDepths); //find blobs of a given hue

			//tracked rects
			for (int r = 0; r < trackedRects.size(); r++)
			{
				for (int n = 0; n < newRects.size(); n++)
				{
					if (trackedRects[r].contains(getRectCenterPoint(newRects[n])))
					{
						newRects.erase(newRects.begin() + n);
						continue;
					}
				}
			}

			if (m_Groups[i][m].isOccluded())
			{
				//sigmoid searchradius growth
				double newRadius = 38400/(1 + std::pow(M_E, -1*m_Groups[i][m].framesOccluded()));
				m_Groups[i][m].searchRadius(newRadius); 
			}
	
			if(newRects.size() > 0)
			{
				//todo: add logic for handling occlusions when marker number > blob number				
				int bIndex = 0;
				double minDistance = getDistance(predictedPt, getCentroid(newRects[bIndex], newDepths[bIndex]));

				for(int b = 1; b < newRects.size(); b++)
				{
					//blob regions not associated with a marker
					double newDistance = getDistance(predictedPt, getCentroid(newRects[b], newDepths[b]));
					if(newDistance < minDistance)
					{
						minDistance = newDistance; //the new minumum
						bIndex = b; //which index
					}
				}
				//BruteForce LAP

				//minDistance should not be constant. should adapt to the mean distance of the marker from the predictions
				
				if(minDistance < m_Groups[i][m].searchRadius())
				{
					m_Groups[i][m].update(newRects[bIndex], newDepths[bIndex]);
					m_Groups[i][m].searchRadius(10000.0);
					m_Groups[i][m].resetOcclusion();

					trackedRects.push_back(newRects[bIndex]);
					
				}
				else
				{
					m_Groups[i][m].raiseOcclusion();
				}

			}
			else
			{
							
					m_Groups[i][m].raiseOcclusion();
			}
			
		}
	}
}

void MarkerTracker::adaptiveTrack(const Mat &hsvImg, const Mat &depthMap)
{
	FilterImage(hsvImg, m_blur);

	Mat hueMat, trackedRegion;

	float hranges[] = {0,180};
    const float *phranges = hranges;

	int ch[] = {0, 0};
	hueMat.create(hsvImg.size(), hsvImg.depth());
	mixChannels(&hsvImg, 1, &hueMat, 1, ch, 1);
	
	//#pragma omp parallel for shared(hsvImg)
	for(int i = 0; i < m_Groups.size(); i++)
	{
		//iterate over the stored hueValuse
		//segment the image based on depth
		Mat depthMask = ThresholdDepth(depthMap);
		vector<Rect> trackedRects;

		for(int m = 0; m < m_Groups[i].size(); m++)
		{
			Mat  hueMask, finalMask, backproj;

			hsvImg.copyTo(hueMask, depthMask);
			finalMask = ThresholdHSV(m_Groups[i][m].markerHSV(), hueMask);

			for (int r = 0; r < trackedRects.size(); r++)
			{
				Mat roi(finalMask, trackedRects[r]);
				roi = Scalar(0);
			}

			Rect trackWindow;
			RotatedRect trackBox;

			m_Groups[i][m].predict();
			Point3d predictedPt = m_Groups[i][m].predictedCenter();

			if (m_Groups[i][m].isOccluded())
			{
				//sigmoid searchradius growth
				double radius = 38400/(1 + std::pow(M_E, -1*m_Groups[i][m].framesOccluded()));
				m_Groups[i][m].searchRadius(radius); 

				trackWindow = getRectfromCenter(Point(predictedPt.x, predictedPt.y), 
					radius/100, radius/100);


				calcBackProject(&hueMat, 1, 0, m_Groups[i][m].markerHist(), backproj, &phranges);
				backproj &= finalMask;

				trackBox = CamShift(backproj, trackWindow,
											TermCriteria( CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 10, 1 ));

			}
			else
			{

				trackWindow = m_Groups[i][m].rect();


				calcBackProject(&hueMat, 1, 0, m_Groups[i][m].markerHist(), backproj, &phranges);
				backproj &= finalMask;

				trackBox = CamShift(backproj, trackWindow,
											TermCriteria( CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 10, 1 ));

			}

			
			//cout<<trackWindow.area()<<endl;
			if( trackBox.boundingRect().area() < m_lowpass ) //lowpass threshold
			{
				m_Groups[i][m].raiseOcclusion();
				//trackWindow uses searchradius size and predicted x, y from kalman
			}
			else
			{
				//succesful detection?
				vector<int> depth_blob = extractDepth(trackWindow, depthMap);
				m_Groups[i][m].update(trackBox, trackWindow, vectorMedian(depth_blob));
				m_Groups[i][m].searchRadius(10000.0);
				m_Groups[i][m].resetOcclusion();

				trackedRects.push_back(trackWindow);

			}
			

		}
	}
}

void MarkerTracker::munkresTrack(const Mat &hsvImg, const Mat &depthMap)
{

	FilterImage(hsvImg, m_blur); //filter

	//#pragma omp parallel for shared(hsvImg)
	for(int i = 0; i < m_Groups.size(); i++)
	{
		Mat depthMask, hueMask, binary;

		//segment the image based on color
		depthMask = ThresholdDepth(depthMap); //remove unwanted depth values
		hsvImg.copyTo(hueMask, depthMask); //mask
		binary = ThresholdHue(m_hueValues[i], hueMask); 
		//creates thresholded binary

		vector<Rect> targetRects; //newly returned rects
	    vector<double> targetDepths; //new center of masses( mass z for a given rect)

		DetectObjects(binary, depthMap, targetRects, targetDepths);

		int nMarkers = m_Groups[i].size();
		int nTargets = targetRects.size();

		//initialize matrix for Hungarian LAP
		vector< vector<int> > distanceMatrix(nMarkers, vector<int>(nTargets, 0));
		cout<<nTargets<<" "<<nMarkers<<endl;
		if(nTargets > 0)
		{
			for(int m = 0; m < nMarkers; m++)
			{
				m_Groups[i][m].raiseOcclusion();
				m_Groups[i][m].predict();
				
				for(int b = 0; b < nTargets; b++)
				{
					//blob regions not associated with a marker
					Point3d targetPos = getCentroid(targetRects[b], targetDepths[b]);
					int targetSize    = targetRects[b].area();
					distanceMatrix[m][b] = m_Groups[i][m].estimateCost(targetPos, targetSize);
				}
			}

			Hungarian hungarian( distanceMatrix, nMarkers, nTargets, HUNGARIAN_MODE_MINIMIZE_COST);


			 // hungarian_print_costmatrix(&hungarian);

			/* solve the assignement problem */
			hungarian.solve();
			  //assign markers to targets, need a bit more of logic here

			  for (int x = 0; x < nMarkers; x++)
			  {
					for (int y = 0; y < nTargets; y++)
					{
						if (hungarian.assignment()[x][y] == 1)
						{
							int mInd = x; //Marker Index for this pair
							int tInd = y; //target Index for this pair

							//need limiter here, and handle when there are more markers than targerts
							m_Groups[i][mInd].update(targetRects[tInd], targetDepths[tInd]);
						}
					}
			  }


			  // free used memory

		}
		else
		{
			for(int m = 0; m < nMarkers; m++)
			{
				m_Groups[i][m].raiseOcclusion();
			}
		}

	}
}


//INIT DETECTOR
void MarkerTracker::SeedObject(Point seed, const Mat &hsvImg, const Mat &depthMap, vector <vector<Point2d>> &blobs, vector<Rect> &rects, vector<double> &depth, Scalar hue)
{
	Mat label_image;

	Mat binary = ThresholdHue(hue, hsvImg);
	binary.convertTo(label_image, CV_32FC1);

	int label_count = 2; // starts at 2 because 0,1 are used already
 
	if(label_image.at<float>(seed) != 0) 
	{

		Rect newRect;
		floodFill(label_image, seed, Scalar(label_count), 
						&newRect, Scalar(0), Scalar(0), 8);

						
		vector<Point2d> image_blob;
		vector<int> depth_blob;
 
		for(int i=newRect.y; i < (newRect.y+newRect.height); i++) 
		{
			for(int j=newRect.x; j < (newRect.x+newRect.width); j++) 
			{
				if(label_image.at<float>(i,j) != label_count) 
				{
					continue;
				}

				image_blob.push_back(Point2d(j,i));
				depth_blob.push_back(depthMap.at<__int16>(i,j));
			}
		}
	
		//if (newRect.area() > m_lowpass && newRect.area() < m_highpass)
		if (image_blob.size() > m_lowpass && image_blob.size() < m_highpass)
		{
			blobs.push_back(image_blob);
			rects.push_back(newRect);
			depth.push_back(vectorMedian(depth_blob));
		}
	}
}

void MarkerTracker::SeedObject(Point seed, const Mat &binary, const Mat &hsvImg, const Mat &depthMap, vector<Rect> &rects, vector<double> &depth, Scalar hsv)
{
	Mat label_image;

	binary.convertTo(label_image, CV_32FC1);

	int label_count = 2; // starts at 2 because 0,1 are used already
 
	if(label_image.at<float>(seed) != 0) 
	{

		Rect newRect;
		floodFill(label_image, seed, Scalar(label_count), 
						&newRect, Scalar(0), Scalar(0), 8);

		vector<int> depth_blob;
 
		for(int i=newRect.y; i < (newRect.y+newRect.height); i++) 
		{
			for(int j=newRect.x; j < (newRect.x+newRect.width); j++) 
			{
				if(label_image.at<float>(i,j) != label_count) 
				{
					continue;
				}

				depth_blob.push_back(depthMap.at<__int16>(i,j));
			}
		}
	
		if (newRect.area() > m_lowpass && newRect.area() < m_highpass)
		{
			rects.push_back(newRect);
			depth.push_back(vectorMedian(depth_blob));
		}
	}
}


//INITIALIZATION FUNCTIONS
void MarkerTracker::add(Point seed, const Mat &hsvImg, const Mat &depthMap)
{
	vector<vector<Point2d>> blobs;
	vector<Rect> rects;
	vector<double> depths;

	FilterImage(hsvImg);

	Scalar hsv = getHSV(seed, hsvImg);
	size_t hGroup = 0;

	bool isContained = false;
	cout<< "Value picked :" << hsv[0] <<endl;

	Mat mask = ThresholdHSV(hsv, hsvImg);
	SeedObject(seed, mask, hsvImg, depthMap, rects, depths, hsv);
	
	if(rects.size() > 0)//we found a blob/boundingrect
	{
		for( int h = 0; h < m_hueValues.size(); h++) //check to see if we already have this color selected, then add the marker to the color group
		{
			if(abs(hsv[0] - m_hueValues[h][0]) < m_High)
			{
				hGroup = h;
				isContained = true;
				cout<<"Existing color, adding to group."<<endl;
			}
		}

		if(isContained) //if we have that group, just add the marker to the existing group
		{
			if (m_camshift)
			{
				Mat hist = CreateHist(rects[0], mask, hsvImg);
				Marker marker(rects[0], depths[0], hsv, hist, m_slidingFrames, m_markerCount);
				m_Groups[hGroup].push_back(marker);
			}
			else
			{
				Marker marker(rects[0], depths[0], hsv, m_slidingFrames, m_markerCount);
				m_Groups[hGroup].push_back(marker);
			}

			
		}
		else //if we don't, make a new group, add to the hueValue vector, and then add the marker to that group
		{
			vector<Marker> markerGroup; //create new markergroup
			hGroup = m_hueValues.size();
			m_hueValues.push_back(hsv[0]);
			cout << "New hue: "<< hsv[0] <<endl;

			if (m_camshift)
			{
				Mat hist = CreateHist(rects[0], mask, hsvImg);
				Marker marker(rects[0], depths[0], hsv, hist, m_slidingFrames, m_markerCount);
				markerGroup.push_back(marker); //add marker to marker group (based on histogram)
			}
			else
			{
				Marker marker(rects[0], depths[0], hsv, m_slidingFrames, m_markerCount);  //always going to be one blob as we run floodfill with a single seed
				markerGroup.push_back(marker); //add marker to marker group (based on color)
			}

			
			m_Groups.push_back(markerGroup);

		}
		m_markerCount++;
	}
	else
	{
		cout<<"no marker"<<endl;
	}

}

//ACCESSORS

const vector<vector<Marker>>& MarkerTracker::groups() const{
	return m_Groups;
}

int MarkerTracker::markerCount()  const{
	return m_markerCount;
}

bool MarkerTracker::usingCamshift() const{
	return m_camshift;
}