/*
MONKINECT

OpenCV - Kinect app for 3D marker-based motion capture of primate locomotion.

David A. Schwarz
Nicolelis Lab

I guess I need to carry over OpenCV, so this software is licensed under BSD. (C) David Schwarz, 2013.

*/

#include "Sockets.h"
#include "MarkerTracker.h"
#include "opencv2/highgui/highgui.hpp"
#include "MarkerBuffer.pb.h"
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <libconfig.h++>

#include <iostream>
#include <fstream>
#include <time.h>
#include <sstream>
#include <sys/stat.h>

using namespace cv;
using namespace std;
using namespace libconfig;

struct cb_param{
	int *ID;
	Mat *hsvImg;
	Mat *depthMap;
	MarkerTracker *mkTracker;
};

struct text_param{
	int fontFace;
	double fontScale;
	int thickness;  
};

template <class T>
inline std::string to_string (const T& t)
{
	std::stringstream ss;

	ss << t;

	return ss.str();
}

template <class T1, class T2>
inline std::string ts_pack(const T1& t1, const T2& t2)
{
	std::stringstream ts;

	ts << t1 << " " << t2;

	return ts.str();

}

template <typename TMessage> 
bool serialize_delimited(std::ostream &stream, TMessage &message) 
{ 
        assert(message.IsInitialized());
        google::protobuf::io::OstreamOutputStream ostreamWrapper(&stream); 
        google::protobuf::io::CodedOutputStream codedOStream(&ostreamWrapper); 
        // Write the message size first. 
        int messageSize = message.ByteSize(); 
        assert(messageSize > 0); 
        codedOStream.WriteLittleEndian32(messageSize); 
        // Write the message. 
        message.SerializeWithCachedSizes(&codedOStream); 

        return stream.good(); 
} 

void help()
{
        cout << "\n Monkinect: 3D motion capture of primate movement with markers and kinect."
 
			"Use:\n"
			"Pressing f writes to file (off by default) \n"
			"Pressing n starts a new file (for new sessions) \n"
			"Pressing p toggles sockets (on by default) \n"
			"Pressing r reloads config file (if changes are made) \n"
			"Pressing w captures to video file (pops up codec selection screen, blocking call) \n"

         << endl;
}

void printCommandLineParams()
{
    cout << "-mode     image mode: resolution and fps, supported three values:  0 - CV_CAP_OPENNI_VGA_30HZ, 1 - CV_CAP_OPENNI_SXGA_15HZ," << endl;
    cout << "          2 - CV_CAP_OPENNI_SXGA_30HZ (0 by default). " << endl;
    cout << "          By default -m 110 i.e. disparity map and rgb image will be shown." << endl ;
    cout << "-r        Filename of .oni video file. The data will grabbed from it." << endl ;
	cout << "-cam       Camera to read images from (0 or 1; 0 by default)" <<endl;
	cout << "-I, -conf		Config file to use (default is monkinect.cfg)"<<endl;
	cout << "-ds		Downsample?" <<endl;
}

void printDeviceSettings(VideoCapture &capture)
{

	cout << "\nDepth generator output mode:" << endl <<
            "FRAME_WIDTH      " << capture.get( CV_CAP_PROP_FRAME_WIDTH ) << endl <<
            "FRAME_HEIGHT     " << capture.get( CV_CAP_PROP_FRAME_HEIGHT ) << endl <<
            "FRAME_MAX_DEPTH  " << capture.get( CV_CAP_PROP_OPENNI_FRAME_MAX_DEPTH ) << " mm" << endl <<
            "FPS              " << capture.get( CV_CAP_PROP_FPS ) << endl <<
            "REGISTRATION     " << capture.get( CV_CAP_PROP_OPENNI_REGISTRATION ) << endl;
    if( capture.get( CV_CAP_OPENNI_IMAGE_GENERATOR_PRESENT ) )
    {
        cout <<
            "\nImage generator output mode:" << endl <<
            "FRAME_WIDTH   " << capture.get( CV_CAP_OPENNI_IMAGE_GENERATOR+CV_CAP_PROP_FRAME_WIDTH ) << endl <<
            "FRAME_HEIGHT  " << capture.get( CV_CAP_OPENNI_IMAGE_GENERATOR+CV_CAP_PROP_FRAME_HEIGHT ) << endl <<
            "FPS           " << capture.get( CV_CAP_OPENNI_IMAGE_GENERATOR+CV_CAP_PROP_FPS ) << endl;
    }
    else
    {
        cout << "\nDevice doesn't contain image generator." << endl;
    }
}

void parseCommandLine( int argc, char* argv[], int& imageMode, int& downsample,
                       string& filename, string& config, bool& isFileReading, int& camNumber )
{
    // set defaut values
	camNumber = 1;
    imageMode = 0;
	downsample = 0;

    filename.clear();
    isFileReading = false;
	
	config = "monkinect.cfg";

    if( argc == 1 )
    {
        help();
    }
    else
    {
        for( int i = 1; i < argc; i++ )
        {
            if( !strcmp( argv[i], "--help" ) || !strcmp( argv[i], "-h" ) )
            {
                printCommandLineParams();
                exit(0);
            }
            else if( !strcmp( argv[i], "-mode" ) )
            {
                imageMode = atoi(argv[++i]);
            }
            else if( !strcmp( argv[i], "-r" ) )
            {
                filename = argv[++i];
                isFileReading = true;
            }
			else if( !strcmp( argv[i], "-I" ) || !strcmp(argv[i], "-conf"))
            {
				config.clear();
                config = argv[++i];
            }
			else if( !strcmp( argv[i], "-cam" ) )
            {
                camNumber = atoi(argv[++i]);
			}
			else if( !strcmp( argv[i], "-ds" ) )
            {
                downsample = atoi(argv[++i]);
            }


            else
            {
                cout << "Unsupported command line argument: " << argv[i] << "." << endl;
                exit(-1);
            }
        }
    }
}

void addBlob(Point seed, const Mat &hsvImg, const Mat &depthMap, MarkerTracker &mkTracker)
{
	mkTracker.add(seed, hsvImg, depthMap);
}

void clearBlob(Point seed, const Mat &hsvImg, MarkerTracker &mkTracker)
{
	//TODO: Spot deletion (currently clearing all)
	mkTracker.clear();
}

void onMouse(int event, int x, int y, int, void *params)
{
	if (event == CV_EVENT_LBUTTONDOWN)
	{
	struct cb_param* parameters = (struct cb_param*) params; //deferences void pointer (make hella sure you only pass the struct) to the struct it deserves
	Point2d seed = Point2d(x,y);
	Mat hsv = *(parameters->hsvImg);
	// (deference the structure member pointer to the pointer of the Mat or the vector)
	addBlob(seed, *(parameters->hsvImg), *(parameters->depthMap), *(parameters->mkTracker));

	}
	else if (event == CV_EVENT_RBUTTONDOWN)
	{
		struct cb_param *parameters = (struct cb_param*) params;
		Point seed = Point(x,y);
		clearBlob(seed, *(parameters->hsvImg), *(parameters->mkTracker));
	}
	else
		return;

}

Scalar* randomColors (const int nColors)
{

	Scalar* colorArray = new Scalar [nColors];

	for(size_t i=0; i < nColors; i++)
		{
			colorArray[i][0] = 255 * (rand()/(1.0 + RAND_MAX));
			colorArray[i][1] = 255 * (rand()/(1.0 + RAND_MAX));
			colorArray[i][2] = 255 * (rand()/(1.0 + RAND_MAX));
			colorArray[i][3] = 255 * (rand()/(1.0 + RAND_MAX));
	    }

	return colorArray;
}

void pb_AddMarker(MarkerBuffer::Marker* buffer, const Marker &marker)
{
	buffer->set_id((int)marker.ID());
	buffer->set_x(marker.center().x);
	buffer->set_y(marker.center().y);

	buffer->set_z(marker.center().z);
	buffer->set_occluded(marker.isOccluded());

	buffer->set_size(marker.size());
	buffer->set_hue(marker.markerHSV()[0]);

}

void pb_AddFilteredMarker(MarkerBuffer::Marker* buffer, const Marker &marker)
{
	buffer->set_id((int)marker.ID());
	buffer->set_x(marker.filteredCenter().x);
	buffer->set_y(marker.filteredCenter().y);
	buffer->set_z(marker.filteredCenter().z);
	buffer->set_occluded(marker.isOccluded());

	buffer->set_size(marker.size());
	buffer->set_hue(marker.markerHSV()[0]);

}


MarkerBuffer::Tracker pb_Output(const MarkerTracker &mkTracker, double timestamp, int64 frame, int cam, bool sendFiltered = false)
{
	/* Function using google protocol buffers to iterate through a set of markers and serialize them to send through UDP. Also log.
	Requies PB 2.4.1 or later
	*/
	MarkerBuffer::Tracker trackerBuffer;
	MarkerBuffer::Tracker::Header* header = trackerBuffer.mutable_header();

	header->set_timestamp(timestamp);
	header->set_frame(frame);
	header->set_markercount(mkTracker.markerCount());
	header->set_camera(cam);

	for(size_t m=0; m < mkTracker.groups().size(); m++)
	{
		for(size_t n=0; n < mkTracker.groups()[m].size(); n++)
		{
			if(sendFiltered)
			{
				pb_AddFilteredMarker(trackerBuffer.add_marker(), mkTracker.groups()[m][n]);
			}
			else
			{
				pb_AddMarker(trackerBuffer.add_marker(), mkTracker.groups()[m][n]);
			}
		}
	}
	return trackerBuffer;
}

std::string str_output(const MarkerTracker &mkTracker, double timestamp, int64 frame, int cam, bool sendFiltered = false)
{
	std::ostringstream stream;
	stream << timestamp << ";";
	for(size_t m=0; m < mkTracker.groups().size(); m++)
	{
		for(size_t n=0; n < mkTracker.groups()[m].size(); n++)
		{
			Marker marker = mkTracker.groups()[m][n];
			stream << cam << ";"
				<< marker.ID() << ";"
				<< marker.center().x << ";" 
				<< marker.center().y << ";" 
				<< marker.center().z << ";" ;
		}
	}
	stream.flush();
	return stream.str();
}

std::string generateFilename(int session, int cam, int TYPE = 0)
{
	time_t timer = time(0);
	struct tm * date = localtime( &timer);

	string ext;

	switch(TYPE)
	{
		case 0:
			ext = "_MARKERS.txt";
			break;
		case 1:
			ext = "_VIDEO.avi";
			break;
		case 2:
			ext = "_LOGFILE.txt";
			break;
		case 3:
			ext = "_DEPTH.avi";
		default:
			ext = "_MARKERS.txt";
			break;
	}
	
	//string format: year-month-day_cam#_session#.bin
	string logname =  to_string(date->tm_year + 1900) + "-" + to_string(date->tm_mon+1) + "-" + to_string(date->tm_mday) + "_cam-" + to_string(cam) 
		+ "_session-" + to_string(session) + ext;

	return logname;
}

bool fileExists(const std::string& filename)
{
    struct stat buf;
    if (stat(filename.c_str(), &buf) != -1)
    {
        return true;
    }
    return false;
}

void display(Mat &output, MarkerTracker &mkTracker, Scalar* &colors, text_param &textDisp)
{

	for(size_t m=0; m < mkTracker.groups().size(); m++)
	{
		for(size_t n=0; n < mkTracker.groups()[m].size(); n++)
		{
			Point Center = Point(mkTracker.groups()[m][n].center().x, mkTracker.groups()[m][n].center().y);
			Point PredCenter = Point(mkTracker.groups()[m][n].predictedCenter().x, mkTracker.groups()[m][n].predictedCenter().y);

			if(mkTracker.groups()[m][n].isOccluded())
			{continue;}

			if(mkTracker.usingCamshift())
			{
				ellipse(output, mkTracker.groups()[m][n].rotatedRect(), colors[mkTracker.groups()[m][n].ID()], 3, 8);
			}
			else
			{
				rectangle(output, mkTracker.groups()[m][n].rect(), colors[mkTracker.groups()[m][n].ID()], 3 ,8, 0);
			}
			circle( output, Center, 4, colors[mkTracker.groups()[m][n].ID()] , -1, 8, 0);
			circle( output, PredCenter, 4, Scalar( 255, 0, 0 ), -1, 8, 0);

			putText(output, to_string(mkTracker.groups()[m][n].ID()), Center, textDisp.fontFace, textDisp.fontScale, Scalar::all(255), textDisp.thickness,8);
			//putText(output, to_string(mkTracker.groups()[m][n].ID()), Center, fontFace, fontScale, Scalar::all(255), thickness,8);
		}
					
	}
}

void createConfig()
{
	//create a new config with default values
	static const char *output_file = "monkinect.cfg";
	Config cfg;

	Setting &root = cfg.getRoot();

	Setting &tracker = root.add("tracker", Setting::TypeGroup);
	tracker.add("skip", Setting::TypeInt) = 10;
	tracker.add("lowpass", Setting::TypeInt) = 500;
	tracker.add("highpass", Setting::TypeInt) = 50000;

	tracker.add("open", Setting::TypeInt) = 1;
	tracker.add("close", Setting::TypeInt) = 2;

	tracker.add("blur", Setting::TypeInt) = 0;
	tracker.add("window", Setting::TypeInt) = 5;

	tracker.add("camshift", Setting::TypeBoolean) = false;
	tracker.add("codec", Setting::TypeString) = "WMV1";

	Setting &hue = tracker.add("hue", Setting::TypeGroup);
	hue.add("low", Setting::TypeInt) = 4;
	hue.add("high", Setting::TypeInt) = 4;

	Setting &saturation = tracker.add("saturation", Setting::TypeGroup);
	saturation.add("min", Setting::TypeInt) = 100;
	saturation.add("max", Setting::TypeInt) = 255;
	saturation.add("low", Setting::TypeInt) = 35;
	saturation.add("high", Setting::TypeInt) = 35;

	Setting &value = tracker.add("value", Setting::TypeGroup);
	value.add("min", Setting::TypeInt) = 100;
	value.add("max", Setting::TypeInt) = 255;
	value.add("low", Setting::TypeInt) = 35;
	value.add("high", Setting::TypeInt) = 35;

	Setting & depth = tracker.add("depth", Setting::TypeGroup);
	depth.add("min", Setting::TypeInt) = 500;
	depth.add("max", Setting::TypeInt) = 2000;

	Setting &marker = root.add("marker", Setting::TypeGroup);
	marker.add("radius", Setting::TypeFloat) = 10000.0;
	marker.add("maxradius", Setting::TypeFloat) = 50000.0;

	Setting &network = root.add("network", Setting::TypeGroup);

	Setting &transfer = network.add("transfer", Setting::TypeGroup);
	transfer.add("active", Setting::TypeBoolean) = true;
	transfer.add("sendFiltered", Setting::TypeBoolean) = false; //sends median filtered values
	transfer.add("address", Setting::TypeString) = "152.16.229.53";
	transfer.add("port", Setting::TypeInt) = 8846;

	Setting &sync = network.add("sync", Setting::TypeGroup);
	sync.add("active", Setting::TypeBoolean) = true;
	sync.add("address", Setting::TypeString) = "152.16.229.37";
	sync.add("port", Setting::TypeInt) = 8845;



	  try
	  {
		cfg.writeFile(output_file);
		cerr << "New configuration successfully written to: " << output_file
			 << endl;

	  }
	  catch(const FileIOException &fioex)
	  {
		cerr << "I/O error while writing file: " << output_file << endl;
	  }

}

void initialize(Config &cfg, const char *confile)
{
	for(;;)
	{
		  try
		  {
			cfg.readFile(confile);
			cout<<"Succesfully read file "<< confile << endl;
			return;
		  }
		  catch(const FileIOException &fioex)
		  {
			cerr << "I/O error while reading file." << endl;
			cerr << "Creating config file with default values." << endl;
			createConfig();
		  }
		  catch(const ParseException &pex)
		  {
			cerr << "Parse error at " << pex.getFile() << ":" << pex.getLine()
					  << " - " << pex.getError() << endl;

			cerr << "Config file corrupted. Creating config file with default values." << endl;
			createConfig();
		  }
	}

}

bool reinitialize(Config &cfg, const char *confile)
{
	for(;;)
	{
		try
		{
			initialize(cfg, confile);
			return true;
		}
		catch(int e)
		{
			cout<<"Initializing failed. Exception "<<e<<endl;
			return false;
		}	
	}

}

/*
 * To work with Kinect or XtionPRO the user must install OpenNI library and PrimeSensorModule for OpenNI and
 * configure OpenCV with WITH_OPENNI flag is ON (using CMake).
 */
int main( int argc, char* argv[] )
{
// Verify that the version of the library that we linked against is
  // compatible with the version of the headers we compiled against. Crashes otherwise.
	GOOGLE_PROTOBUF_VERIFY_VERSION;

	//COMMAND LINE
    int imageMode, downsample, camNumber;
    string filename, config;
    bool isVideoReading;

    parseCommandLine( argc, argv, imageMode, downsample, filename, config, isVideoReading, camNumber );

	//CONFIGURATION
	//const char *confile = "monkinect.cfg"; //static atm
	Config cfg;
	initialize(cfg, config.c_str()); //try config file
	Scalar* colors = randomColors(50);

	//TEXT
	text_param textDisp;
	textDisp.fontFace = FONT_HERSHEY_SCRIPT_SIMPLEX;
	textDisp.fontScale = 1;
	textDisp.thickness = 1;  

	//NETWORK
	bool fullTransfer = false;
	bool sendFiltered = false;
	//Ugly syntax but compact
	cfg.getRoot()["network"]["transfer"].lookupValue("active", fullTransfer); 
	cfg.getRoot()["network"]["transfer"].lookupValue("sendFiltered", sendFiltered); 
	UDPSocketClient transferSocket(cfg, "transfer");
	UDPSocketClient syncSocket(cfg, "sync");
	
	//LOG
	bool writeToFile      = false; //do not write yet
	bool syncToSocket     = true;
	bool transferToSocket = true; //do send packets
	bool newFileFlag      = true; //is this a new session
	int  trackRun         = 0; //number of current session/run


	//FPS
	time_t frameStart, frameEnd;
	int64 frameCounter = 0; //for long term recording

	//LOGFILE
	ofstream markerfile;
	ofstream logfile;

	logfile.open(generateFilename(trackRun, camNumber,2), ios::out | ios::app);
	logfile << "Monkinect ver 1.1a" << endl;


	//Vectors that will hold camera variables (quickly scale).
	vector<Mat> colorImages; //8 bit unsigned int
	vector<Mat> hsvImages;  //8 bit unsigned int
	vector<Mat> depthImages; //16 bit unsigned 
	vector<int> ints;
	vector<MarkerTracker> tracker; //hold tracker class

	vector<cb_param> parameters;
	vector<void*> pointerVec;

	//video capture
	vector<VideoCapture> capture;
	vector<VideoWriter> videoWriter;
	vector<VideoWriter> depthWriter;
	vector<string> windowNames;
	string cc;
	cfg.getRoot()["tracker"].lookupValue("codec", cc);
	if(cc.empty() || cc.size() > 4)
	{
		cc = "WMV1";
	}

	int videoRun = 0;
	//initialize all available cameras
	//Dynamically set the containers
	for (int c = 0; c < camNumber; c++)
	{

		cout << "Device #" << c << " opening ..." << endl;
		VideoCapture cap;
		VideoWriter write;
		VideoWriter depth;

		//LIVE-FEED or .ONI, if I could figure out how to trigger

		if( isVideoReading )
			cap.open( filename );
		else
			{
			 cap.open( CV_CAP_OPENNI + c );
			}

		cout << "done." << endl;

		if( !cap.isOpened() )
		{
			cout << "Can not open a capture object." << endl;
			return -1;
		}


		if( !isVideoReading )
		{
			bool modeRes=false;
			switch ( imageMode )
			{
				case 0:
					modeRes = cap.set( CV_CAP_OPENNI_IMAGE_GENERATOR_OUTPUT_MODE, CV_CAP_OPENNI_VGA_30HZ );
					logfile << "capture mode: VGA_30HZ" << endl;
					break;
				case 1:
					modeRes = cap.set( CV_CAP_OPENNI_IMAGE_GENERATOR_OUTPUT_MODE, CV_CAP_OPENNI_SXGA_15HZ );
					logfile << "capture mode: SXGA_15HZ" << endl;
					break;
				case 2:
					modeRes = cap.set( CV_CAP_OPENNI_IMAGE_GENERATOR_OUTPUT_MODE, CV_CAP_OPENNI_SXGA_30HZ );
					logfile << "capture mode: SXGA_30HZ" << endl;
					break;
					//need to patch OpenCV in order to use 60Hz
				default:
					CV_Error( CV_StsBadArg, "Unsupported image mode property.\n");
			}
			if (!modeRes)
			{
				cout << "\nThis image mode is not supported by the device, the default value (CV_CAP_OPENNI_VGA_30HZ) will be used.\n" << endl;
				modeRes = cap.set( CV_CAP_OPENNI_IMAGE_GENERATOR_OUTPUT_MODE, CV_CAP_OPENNI_VGA_30HZ );
			}
		}

		// Print some avalible device settings.
		printDeviceSettings(cap);
		capture.push_back(cap);
		videoWriter.push_back(write);
		depthWriter.push_back(depth);

		//IMAGES
		Mat hsvImage, depthMap;
		hsvImages.push_back(hsvImage);
		depthImages.push_back(depthMap);

		//TRACKER
		MarkerTracker mkTracker(cfg);
		tracker.push_back(mkTracker);

		//CALLBACK
		cb_param param;
		parameters.push_back(param);

	}
	
	//Set the pointers. std::vector pointers are guaranteed unless the vector is resized
	//Thus, the camera container vectors should not be resized. (do not use lists, either)...
	for (int c = 0; c < camNumber; c++)
	{
		parameters[c].ID        = &ints[c];
		parameters[c].hsvImg    = &hsvImages[c];
		parameters[c].depthMap  = &depthImages[c];
		parameters[c].mkTracker = &tracker[c];

		void* pVoid = &parameters[c];
		pointerVec.push_back(pVoid);

		std::string windowName = "Monkinect" + to_string(c);
		namedWindow( windowName, 0 );

		setMouseCallback(windowName, onMouse, pointerVec[c]);

		windowNames.push_back(windowName);

	}


	time(&frameStart);
	
	logfile << "Program started." << endl;

	double timerStart = clock();
	double timerCurrent = timerStart/CLOCKS_PER_SEC;

    for(;;)
	{
		ostringstream outstream;
		string timestamp;
		
		for (int cam = 0; cam < camNumber; cam++)
		{
			Mat bgrImage;

			if( !capture[cam].grab() )
			{
				cout << "Unit #"<< cam << " can not grab images." << endl;
				return -1;
			}
			else
			{
				if(capture[cam].retrieve(bgrImage, CV_CAP_OPENNI_BGR_IMAGE) && capture[cam].retrieve(depthImages[cam], CV_CAP_OPENNI_DEPTH_MAP ))
				{
					//RESIZE?
					if (downsample > 0)
					{
						resize(bgrImage, bgrImage, Size(), 0.5, 0.5, INTER_AREA);
						resize(depthImages[cam], depthImages[cam], Size(), 0.5, 0.5, INTER_AREA);
					}

					//PROCESS

					Mat output = bgrImage;

					cvtColor(bgrImage, hsvImages[cam], CV_BGR2HSV);

					if(tracker[cam].usingCamshift())
					{
						tracker[cam].adaptiveTrack(hsvImages[cam], depthImages[cam]);
					}
					else
					{
						tracker[cam].munkresTrack(hsvImages[cam], depthImages[cam]);
					}

					display(output, tracker[cam], colors, textDisp);

					//timing things



				if(transferToSocket)
				{
					if (fullTransfer)
					{
						serialize_delimited(outstream, pb_Output(tracker[cam], timerCurrent, frameCounter, cam, sendFiltered));
						outstream.flush();
						transferSocket.SendBytes(outstream.str());
					}
					else
					{
						std::string send = str_output(tracker[cam], timerCurrent, frameCounter, cam, sendFiltered);
						transferSocket.SendBytes(timestamp);
					}
					//network sync
					//now in seconds!
				}

				//FILE
				if(writeToFile)
				{
					markerfile << str_output(tracker[cam], timerCurrent, frameCounter, cam, sendFiltered)<< endl;
					//serialize_delimited(markerfile, pb_Output(tracker[cam], timerCurrent, frameCounter, cam));
				
				}

				//RESULT

				if (videoWriter[cam].isOpened())
				{
					videoWriter[cam].write(output);
					depthWriter[cam].write(depthImages[cam]);
				}
				
				imshow(windowNames[cam], output);
				
				//cout<<parameters[cam].hsvImg[0].dims<<endl;

			}
        }

	}

		double timerEnd = clock();
		time(&frameEnd);

		double ctime = difftime(frameEnd, frameStart);
		int fps = frameCounter/ctime;

		frameCounter++; //frame counter counts all instances of frames as one (as it's synced)
		timerCurrent = timerEnd - timerStart / CLOCKS_PER_SEC;

		//timestamp = ts_pack(timerCurrent, camNumber);
		timestamp = ts_pack(timerCurrent, frameCounter);

		///putText(output, to_string(fps), Point(10,30), textDisp.fontFace, textDisp.fontScale, Scalar::all(255), textDisp.thickness,8);
		//SOCKET
		if(syncToSocket)
		{
			//just sending one message per packet, very inefficient but it'll make do for now.
			//network log
			syncSocket.SendBytes(timestamp);
		}

		char key = cvWaitKey(5);

		if(key==27) 
		{
			markerfile.close();
			break;
		}

		switch(key)
		{
		case 'f':

		writeToFile = !writeToFile;

		if(newFileFlag)
		{
			string checkname = generateFilename(trackRun , camNumber);

			while(fileExists(checkname))
			{
				trackRun++;
				checkname = generateFilename(trackRun , camNumber);
			}

			cout<<"Opening file: "<< checkname <<endl;
			markerfile.open(checkname, ios::out | ios::app);
			logfile << "$" << timerCurrent << ": New marker file " << checkname <<endl;
			newFileFlag = false;
		}

		if(writeToFile)
		{
			cout<<"Writing to markerfile"<<endl;
			logfile << "$" <<  timerCurrent << ": Writing to marker file " <<endl;
		}
		else
		{
			cout<<"Closing markerfile"<<endl;
			logfile << "$" <<  timerCurrent << ": Stopped writing to marker file " <<endl;

		}

		break;

		case 'p':
		transferToSocket = !transferToSocket;
		break;

		case 'n':
			if(markerfile.is_open())
			{
				markerfile.close();
				cout<<"Closing markerfile"<<endl;
				logfile << "$" <<  timerCurrent << ": Closing marker file " <<endl;
			}
			trackRun++;
			cout<<"Opening new markerfile"<<endl;
			logfile << "$" <<  timerCurrent << ": Opening new marker file " <<endl;
			markerfile.open(generateFilename(trackRun, camNumber), ios::out | ios::binary);
		break;

		case 'r':
			cout<<"Reloading config file"<<endl;
			if(reinitialize(cfg, config.c_str()))
			{
				for (int cam = 0; cam < camNumber; cam++)
				{
					tracker[cam].reload(cfg);
					logfile << "$" <<  timerCurrent << ": Reloaded config file " <<endl;
				}
			}
			break;
			

		case 'w':

			if(videoWriter[0].isOpened())
			{
				cout<<"Closing video file"<<endl;
				logfile << "$" <<  timerCurrent << ": Closing video file " << "at frame: " << frameCounter << endl;
				for (int cam = 0; cam < camNumber; cam++)
				{
					videoWriter[cam].release(); //close all writers
					depthWriter[cam].release();
				}
			}
			else
			{
				for (int cam = 0; cam < camNumber; cam++)
				{
					string videoname = generateFilename(videoRun, cam, 1);
					string depthname = generateFilename(videoRun, cam, 3);
					while(fileExists(videoname))
					{
						videoRun++;
						videoname = generateFilename(videoRun , cam, 1);
						depthname = generateFilename(videoRun, cam, 3);
						
					}

					Size size = Size((int) capture[0].get(CV_CAP_PROP_FRAME_WIDTH),    //Acquire input size
					(int) capture[0].get(CV_CAP_PROP_FRAME_HEIGHT));

					//videoWriter.open(videoname, -1, capture.get(CV_CAP_PROP_FPS), size);
						cout << "Using video codec: "<< cc <<endl;
						logfile << "Using video codec: "<< cc <<endl;
						if (videoWriter[cam].open(videoname, CV_FOURCC(cc[0],cc[1],cc[2],cc[3]), capture[0].get(CV_CAP_PROP_FPS), size))
						{
							cout << "#" <<  timerCurrent << ": Writing to video file " << videoname << "at frame: " << frameCounter << endl;
							logfile << "#" <<  timerCurrent << ": Writing to video file " << videoname << "at frame: " << frameCounter << endl;
						}
						else
						{
							cout<<"Could not open video file. Check codec."<<endl;
						}

						if (depthWriter[cam].open(depthname, 0, capture[0].get(CV_CAP_PROP_FPS), size))
						{
							cout << "#" <<  timerCurrent << ": Writing to video file " << depthname << "at frame: " << frameCounter << endl;
							logfile << "#" <<  timerCurrent << ": Writing to video file " << depthname << "at frame: " << frameCounter << endl;
						}
						else
						{
							cout<<"Could not open video files. Check codec."<<endl;
						}

					}
			}
			break;



		case 'q':
		markerfile.close();
		logfile.close();
		for (int cam = 0; cam < camNumber; cam++)
		{
			videoWriter[cam].release();
			depthWriter[cam].release();
		}
		break;
		
		}

    }

    return 0;
}
