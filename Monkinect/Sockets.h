/*
	David Schwarz

	Modification and documentation of the original socket.cpp class (documentation sucked) by Ren� Nyffenegger
	for use in the Nicolelis Lab applications, including recieving BMI packets from MotionBuilder,
	UDK, Kinect, and other applications that require UDP/TCP server functionality.

	Requires libconfig and winsock. 

	Original Socket.h Copyright (C) 2002-2004R Ren� Nyffenegger

	Nicolelis Lab

*/

#include <WinSock2.h>
#include <string>
#include <libconfig.h++>

#ifndef SOCKET_H
#define SOCKET_H

enum TypeSocket {BlockingSocket, NonBlockingSocket};
enum TypeProtocol {TCP, UDP};

class Sockets {
public:

  virtual ~Sockets();
  Sockets(const Sockets&);
  Sockets& operator=(Sockets&);

  std::string ReceiveLine();
  std::string ReceiveBytes();
  int	ReceiveBuffer(char *, int); //returns negative on failure; caller must give it allocated space with size in second argument
//returns bytes received on success (which can be 0)

  void   Close();

  // The parameter of SendLine is not a const reference
  // because SendLine modifes the std::string passed.
  void   SendLine (std::string);

  // The parameter of SendBytes is a const reference
  // because SendBytes does not modify the std::string passed 
  // (in contrast to SendLine).
  void   SendBytes(const std::string&);

protected:
  friend class TCPSocketServer;
  friend class UDPSocketServer;

  friend class UDPSocketClient;
  friend class TCPSocketClient;

  friend class SocketSelect;

  Sockets(SOCKET s);
  Sockets();


  SOCKET s_;

  int* refCounter_;

private:
  static void Start();
  static void End();
  static int  nofSockets_;
};

class UDPSocketClient : public Sockets {
public:
  UDPSocketClient(const std::string& host, int port);
  UDPSocketClient(const libconfig::Config &cfg, const std::string& name = "socket");

};

class TCPSocketClient : public Sockets {
public:
  TCPSocketClient(const std::string& host, int port);
  TCPSocketClient(const libconfig::Config &cfg, const std::string& name = "socket");

};

class TCPSocketServer : public Sockets {
public:
  TCPSocketServer(int port, int connections, TypeSocket type=NonBlockingSocket);
  TCPSocketServer(const libconfig::Config &cfg, const std::string& name = "server");
  Sockets* Accept();
};

class UDPSocketServer : public Sockets {
public:
  UDPSocketServer(int port=8846, TypeSocket type=NonBlockingSocket);
  UDPSocketServer(const libconfig::Config &cfg, const std::string& name = "server");
  Sockets* Bind();

};

// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/winsock/wsapiref_2tiq.asp
class SocketSelect {
  public:
    SocketSelect(Sockets const * const s1, Sockets const * const s2=NULL, TypeSocket type=BlockingSocket);

    bool Readable(Sockets const * const s);

  private:
    fd_set fds_;
}; 



#endif