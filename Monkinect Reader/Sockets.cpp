/*

	David Schwarz

	Modification and documentation of the original socket.cpp class (documentation sucked) by Ren� Nyffenegger
	for use in the Nicolelis Lab applications, including recieving BMI packets from MotionBuilder,
	UDK, Kinect, and other applications that require UDP/TCP server functionality.

	Original Socket.cpp Copyright (C) 2002-2004 Ren� Nyffenegger

	Nicolelis Lab

*/

#include "Sockets.h"
#include <iostream>
#include <stdio.h>



#pragma comment(lib, "ws2_32.lib")

int Sockets::nofSockets_= 0;

void Sockets::Start() {
  if (!nofSockets_) {
    WSADATA info;
    if (WSAStartup(MAKEWORD(2,0), &info)) {
      throw "Could not start WSA";
    }
  }
  ++nofSockets_;
}

void Sockets::End() {
  WSACleanup();
}

//TODO: Add a input for the socket contstructor of SOCKET_TYPE that can be called from main without modifying the cpp code

Sockets::Sockets() : s_(0) {
  Start();

  // UDP: use SOCK_DGRAM instead of SOCK_STREAM
  //s_ = socket(AF_INET,SOCK_STREAM,0);
  s_ = socket(AF_INET,SOCK_DGRAM,0);


  if (s_ == INVALID_SOCKET) {
    throw "INVALID_SOCKET";
  }

  refCounter_ = new int(1);
}

// Socket counting constructor functions
// ---------------------------------------------
Sockets::Sockets(SOCKET s) : s_(s) {
  Start();
  refCounter_ = new int(1);
};

Sockets::~Sockets() {
  if (! --(*refCounter_)) {
    Close();
    delete refCounter_;
  }

  --nofSockets_;
  if (!nofSockets_) End();
}

Sockets::Sockets(const Sockets& o) {
  refCounter_=o.refCounter_;
  (*refCounter_)++; 
  s_         =o.s_;

  nofSockets_++;
}


//When pointer
Sockets& Sockets::operator=(Sockets& o) {
  (*o.refCounter_)++;

  refCounter_=o.refCounter_;
  s_         =o.s_;

  nofSockets_++;

  return *this;
}
//-----------------------------------------------

//Kill socket
void Sockets::Close() {
  closesocket(s_);
}

//Data recieve function. Outputs data to string and returns the string
//TODO: Struct function that recieves data without need for a string parser.

//returns negative on failure, caller must give it allocated space with size in second argument
//returns bytes received on success (which can be 0)
int Sockets::ReceiveBuffer(char *buf, int size) {
  //std::string ret;
  if (buf==NULL) return -1;
  if (size<=0) return -1;
  int counter=0;
 
  while (1) 
  {
    u_long arg = 0;
    if (ioctlsocket(s_, FIONREAD, &arg) != 0) //makesure all the data of socket has arrived
		//sets arg length to a non zero value if the data is non zero
      return -1;

	if (arg == 0) {		
		return counter;
	}

    if (arg + counter> (u_long)size) arg = (u_long)size-counter; //buffer exceeding?
    //pfile = fopen("C:\\myfile.txt","w");
    int rv = recv (s_, &buf[counter], arg, 0);
	counter+=arg;
	//int rv = recvfrom (s_, buf, arg, 0, 0, 0); Recvfrom should go in a separate function that is called by UDP sockets
	//in order to return to sender.
	if (rv <= 0) {
		if (counter>0)
			return counter;
		return -1; //error catch	
	}

  }
  //never gets here
  return -1;
}
std::string Sockets::ReceiveBytes() {
  std::string ret;
  char buf[2048];
 
  while (1) 
  {
    u_long arg = 0;
    if (ioctlsocket(s_, FIONREAD, &arg) != 0) //makesure all the data of socket has arrived
		//sets arg length to a non zero value if the data is non zero
      break;

    if (arg == 0) //above
      break;

    if (arg > 1024) arg = 1024; //buffer exceeding?
    //pfile = fopen("C:\\myfile.txt","w");
    int rv = recv (s_, buf, arg, 0);

	//int rv = recvfrom (s_, buf, arg, 0, 0, 0); Recvfrom should go in a separate function that is called by UDP sockets
	//in order to return to sender.
    if (rv <= 0) break; //error catch

    std::string t;

    t.assign (buf, rv);

	ret += t;
  }


  return ret;
}

std::string Sockets::ReceiveLine() {
  std::string ret;
  while (1) {
    char r;

    switch(recv(s_, &r, 1, 0)) {
      case 0: // not connected anymore;
              // ... but last line sent
              // might not end in \n,
              // so return ret anyway.
        return ret;
      case -1:
        return "";
//      if (errno == EAGAIN) {
//        return ret;
//      } else {
//      // not connected anymore
//      return "";
//      }
    }

    ret += r;
    if (r == '\n')  return ret;
  }
}

void Sockets::SendLine(std::string s) {
  s += '\n';
  send(s_,s.c_str(),s.length(),0);
}

void Sockets::SendBytes(const std::string& s) {
  send(s_,s.c_str(),s.length(),0);
}


/***SOCKET HANDLING CLASSES

//UDPSocketServer is a modification on the TCPSocketServer that mantains the blocking functionality and error catching
//but does away with listen() as UDP is a connection-less protocol

****/

UDPSocketServer::UDPSocketServer(int port, TypeSocket type) {
  sockaddr_in sa;

  memset(&sa, 0, sizeof(sa));

  sa.sin_family = PF_INET;             
  sa.sin_port = htons(static_cast<unsigned short>(port));   
  s_ = socket(AF_INET,SOCK_DGRAM,0);
  if (s_ == INVALID_SOCKET) {
    throw "INVALID_SOCKET";
  }

  if(type==NonBlockingSocket) {
    u_long arg = 1;
    ioctlsocket(s_, FIONBIO, &arg);
  }

  /* bind the socket to the internet address */
  if (bind(s_, (sockaddr *)&sa, sizeof(sockaddr_in)) == SOCKET_ERROR) {
    closesocket(s_);
    throw "INVALID_SOCKET";
  }                              
}

//Passes the UDP Server Sockets into a new instance of Sockets* class
Sockets* UDPSocketServer::Bind() {
  SOCKET new_sock = s_;
  if (new_sock == INVALID_SOCKET) {
    int rc = WSAGetLastError();
    if(rc==WSAEWOULDBLOCK) {
      return 0; // non-blocking call, no request pending
    }
    else {
      throw "Invalid Sockets";
    }
  }

  Sockets* r = new Sockets(new_sock);
  return r;
}

//TCP SocketServer uses IP6 family and builds IP4 socket. Creates a TCP socket, binds it, and calls listen() to wait for incoming TCP connection.
TCPSocketServer::TCPSocketServer(int port, int connections, TypeSocket type) {
  sockaddr_in sa;

  memset(&sa, 0, sizeof(sa));

  sa.sin_family = PF_INET;             
  sa.sin_port = htons(static_cast<unsigned short>(port));   
  s_ = socket(AF_INET,SOCK_STREAM,0);
  if (s_ == INVALID_SOCKET) {
    throw "INVALID_SOCKET";
  }

  if(type==NonBlockingSocket) {
    u_long arg = 1;
    ioctlsocket(s_, FIONBIO, &arg);
  }

  /* bind the socket to the internet address */
  if (bind(s_, (sockaddr *)&sa, sizeof(sockaddr_in)) == SOCKET_ERROR) {
    closesocket(s_);
    throw "INVALID_SOCKET";
  }
  
  listen(s_, connections);                               
}

//Accept calls the accept function, a winsock2 member that creates a new SOCKET from the TCP server listen() socket
//and passes it into a Sockets* class instance, with the "connected" property
Sockets* TCPSocketServer::Accept() {
  SOCKET new_sock = accept(s_, 0, 0);
  if (new_sock == INVALID_SOCKET) {
    int rc = WSAGetLastError();
    if(rc==WSAEWOULDBLOCK) {
      return 0; // non-blocking call, no request pending
    }
    else {
      throw "Invalid Sockets";
    }
  }

  Sockets* r = new Sockets(new_sock);
  return r;
}
//Sockets Client class to be able to switch between sockets, check for readable sockets, and inherit and bind 

//Client handler for UDP
UDPSocketClient::UDPSocketClient(const std::string& host, int port) : Sockets() {
  std::string error;

  hostent *he;
  if ((he = gethostbyname(host.c_str())) == 0) {
    error = strerror(errno);
    throw error;
  }

  sockaddr_in addr;
  addr.sin_family = AF_INET;
  addr.sin_port = htons(static_cast<unsigned short>(port));
  addr.sin_addr = *((in_addr *)he->h_addr);
  memset(&(addr.sin_zero), 0, 8); 

  s_ = socket(AF_INET,SOCK_DGRAM,0);


  if (s_ == INVALID_SOCKET) 
  {
    throw "INVALID_SOCKET";
  }

  refCounter_ = new int(1);


  if (::connect(s_, (sockaddr *) &addr, sizeof(sockaddr))) {
    error = strerror(WSAGetLastError());
    throw error;
  }
}

//Client handler for TCP
TCPSocketClient::TCPSocketClient(const std::string& host, int port) : Sockets() {
  std::string error;

  hostent *he;
  if ((he = gethostbyname(host.c_str())) == 0) {
    error = strerror(errno);
    throw error;
  }

  sockaddr_in addr;
  addr.sin_family = AF_INET;
  addr.sin_port = htons(static_cast<unsigned short>(port));
  addr.sin_addr = *((in_addr *)he->h_addr);
  memset(&(addr.sin_zero), 0, 8); 

  s_ = socket(AF_INET,SOCK_DGRAM,0);


  if (s_ == INVALID_SOCKET) 
  {
    throw "INVALID_SOCKET";
  }

  refCounter_ = new int(1);


  if (::connect(s_, (sockaddr *) &addr, sizeof(sockaddr))) {
    error = strerror(WSAGetLastError());
    throw error;
  }
}


//Function for exchanging sockets.

SocketSelect::SocketSelect(Sockets const * const s1, Sockets const * const s2, TypeSocket type) {
  FD_ZERO(&fds_);
  FD_SET(const_cast<Sockets*>(s1)->s_,&fds_);
  if(s2) {
    FD_SET(const_cast<Sockets*>(s2)->s_,&fds_);
  }     

  TIMEVAL tval;
  tval.tv_sec  = 0;
  tval.tv_usec = 1;

  TIMEVAL *ptval;
  if(type==NonBlockingSocket) {
    ptval = &tval;
  }
  else { 
    ptval = 0;
  }

  if (select (0, &fds_, (fd_set*) 0, (fd_set*) 0, ptval) == SOCKET_ERROR) 
    throw "Error in select";
}

bool SocketSelect::Readable(Sockets const* const s) {
  if (FD_ISSET(s->s_,&fds_)) return true;
  return false;
}